<?php
/**
 * Created by PhpStorm.
 * User: evenvi
 * Date: 16-8-9
 * Time: 上午11:58
 */

$publish_fix_header = 50;


printf("%'08b\n",$publish_fix_header);

$ret = do_get_bit($publish_fix_header);

var_dump($ret);

function do_get_bit($flags)
{
    $retain = ($flags & 1) > 0;
    $qos_level = ($flags & 6) >> 1;
    $dup_flag = ($flags & 8) > 0;
    $message_type = ($flags & 240)>>4;

    return array(
        'retain' => $retain,
        'qos_level' => $qos_level,
        'dup_flag' => $dup_flag,
        'message_type' => $message_type
    );
}

